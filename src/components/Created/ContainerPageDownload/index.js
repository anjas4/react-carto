import { Box } from "@material-ui/core"

const ContainerPageDownload = (props) => {
    const { components } = props
    return (
        <Box>
            {components.map((val, index) => {
                return (
                    <div key={index}>
                        {val}
                    </div>
                )
            })}
        </Box>
    )
}

export default ContainerPageDownload