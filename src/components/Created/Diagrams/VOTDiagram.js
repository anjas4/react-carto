import { TimeSeriesWidgetUI } from "@carto/react-ui"
import { Box } from "@material-ui/core"


const VOTDiagram = () => {
    return (
        <Box width='80%'>
            <TimeSeriesWidgetUI
                data={
                  [
                    {
                      name: 1519599600000,
                      value: 424
                    },
                    {
                      name: 1524434400000,
                      value: 390
                    },
                    {
                      name: 1530482400000,
                      value: 280
                    },
                    {
                      name: 1535320800000,
                      value: 318
                    },
                    {
                      name: 1540767600000,
                      value: 404
                    },
                    {
                      name: 1545606000000,
                      value: 229
                    },
                  ]
                }
                onPause={function noRefCheck(){}}
                onPlay={function noRefCheck(){}}
                onStop={function noRefCheck(){}}
                onTimeWindowUpdate={function noRefCheck(){}}
                onTimelineUpdate={function noRefCheck(){}}
                stepSize="month"
                timeWindow={[]}
            /> 
            
            <TimeSeriesWidgetUI
                data={
                  [
                    {
                      name: 1519599600000,
                      value: 424
                    },
                    {
                      name: 1524434400000,
                      value: 390
                    },
                    {
                      name: 1530482400000,
                      value: 280
                    },
                    {
                      name: 1535320800000,
                      value: 318
                    },
                    {
                      name: 1540767600000,
                      value: 404
                    },
                    {
                      name: 1545606000000,
                      value: 229
                    },
                  ]
                }
                onPause={function noRefCheck(){}}
                onPlay={function noRefCheck(){}}
                onStop={function noRefCheck(){}}
                onTimeWindowUpdate={function noRefCheck(){}}
                onTimelineUpdate={function noRefCheck(){}}
                stepSize="month"
                timeWindow={[]}
            /> 
        </Box>
    )
}

export default VOTDiagram