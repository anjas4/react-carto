import { Box, Typography } from "@material-ui/core"

const PopulationDiagram = () => {
    return (
        <Box margin='auto' border='1px solid red' width='full' height={257} textAlign='center'>
            <Typography>
                Population Distribution diagram
            </Typography>
        </Box>
    )
}

export default PopulationDiagram