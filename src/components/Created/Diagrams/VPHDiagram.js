import { Box, Typography } from "@material-ui/core"

const VPHDiagram = () => {
    return (
        <Box width='full' height={280} border='1px solid red' textAlign='center'>
            <Typography>
                Visitor Per Hour (%)
            </Typography>
        </Box>
    )
}

export default VPHDiagram