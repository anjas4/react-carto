import { CategoryWidgetUI } from "@carto/react-ui"
import { Box, Typography } from "@material-ui/core"


const EconomyDiagram = () => {
    return (
        <Box padding={1} id='economy'>
            <Typography variant='body2'>ECONOMY</Typography>
            <CategoryWidgetUI
                data={[
                {
                    name: 'Low',
                    value: 6000,
                },
                {
                    name: 'Mid-Low',
                    value: 1123,
                },
                {
                    name: 'Mid-High',
                    value: 550,
                },
                {
                    name: 'High',
                    value: 488,
                },

                ]}
                labels={{}}
                onSelectedCategoriesChange={function noRefCheck(){}}
                order="fixed"
            />
        </Box>
    )
}

export default EconomyDiagram