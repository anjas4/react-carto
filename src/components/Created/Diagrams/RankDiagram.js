const { TableWidgetUI } = require("@carto/react-ui")


const TopFive = () => {
    return (
        <TableWidgetUI
            columns={[
                {
                  field: 'location',
                  headerName: 'Top 5',
                  sort: true
                },
                {
                  field: 'totalVisitor',
                  headerName: 'Total Visitor',
                  sort: true,
                  align: 'right'
                },
            ]}
            dense
            onRowClick={function noRefCheck(){}}
            onSetPage={function noRefCheck(){}}
            onSetRowsPerPage={function noRefCheck(){}}
            onSetSortBy={function noRefCheck(){}}
            onSetSortDirection={function noRefCheck(){}}
            rows={[
                {
                    location : '1. Barcelona',
                    totalVisitor: '208,494',
                    id: 1,
                    zip: '41013'
                },

                {
                    location : '2. Toledo',
                    totalVisitor: '189,667',
                    id: 2,
                },

                {
                    location : '3. Valencia',
                    totalVisitor: '166,882',
                    id: 3,
                },

                {
                    location : '4. Coruna',
                    totalVisitor: '117,221',
                    id: 4,
                },

                {
                    location : '5. Potevedra',
                    totalVisitor: '103,997',
                    id: 5,
                },
            ]}
        />
    )
}

const BotFive = () => {
    return (
        <TableWidgetUI
            columns={[
              {
                field: 'location',
                headerName: 'Bottom 5',
                sort: true
              },
              {
                field: 'totalVisitor',
                headerName: 'Total Visitor',
                sort: true,
                align: 'right'
              },
            ]}
            dense
            onRowClick={function noRefCheck(){}}
            onSetPage={function noRefCheck(){}}
            onSetRowsPerPage={function noRefCheck(){}}
            onSetSortBy={function noRefCheck(){}}
            onSetSortDirection={function noRefCheck(){}}
            rows={[
              {
                location : '1. Sona',
                totalVisitor: '9,994',
                id: 1,
                zip: '41013'
              },

              {
                location : '2. Huesca',
                totalVisitor: '9,667',
                id: 2,
              },

              {
                location : '3. Taruel',
                totalVisitor: '5,882',
                id: 3,
              },

              {
                location : '4. Melala',
                totalVisitor: '2,221',
                id: 4,
              },

              {
                location : '5. Cueta',
                totalVisitor: '1,997',
                id: 5,
              },
            ]}
        />
    )
}

export { TopFive, BotFive }