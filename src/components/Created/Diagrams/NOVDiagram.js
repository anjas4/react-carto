const { CategoryWidgetUI } = require("@carto/react-ui")
const { Box, Typography } = require("@material-ui/core")


const NOVDiagram = () => {
    return (
        <Box padding={1} width='30%'>
              <Typography variant='body2'>NUMBER OF VISITER BY TYPE</Typography>
                <CategoryWidgetUI
                  data={[
                    {
                      name: 'President',
                      value: 2227,
                    },
                    {
                      name: 'Comuter',
                      value: 628,
                    },
                    {
                      name: 'Frequently Local',
                      value: 0,
                    },
                    {
                      name: 'Sporadic Local',
                      value: 0,
                    },
                    {
                      name: 'Frequently Nasional',
                      value: 642,
                    },
                    {
                      name: 'Sporadic Nasional',
                      value: 3446,
                    },
                    {
                      name: 'Frequently internasional',
                      value: 36,
                    },
                    {
                      name: 'Sporadic internasional',
                      value: 1180,
                    },

                  ]}
                labels={{}}
                onSelectedCategoriesChange={function noRefCheck(){}}
                order="fixed"
            />
        </Box>
    )
}

export default NOVDiagram