const { PieWidgetUI } = require("@carto/react-ui")
const { Box } = require("@material-ui/core")

const PieDiagram = () => {
    return (
        <Box id="pie">
            <PieWidgetUI
              data={[
                {
                  color: '#7F3C8D',
                  name: 'Women',
                  value: 100
                },
                {
                  color: '#11A579',
                  name: 'Men',
                  value: 100
                }
              ]}
              onSelectedCategoriesChange={function noRefCheck(){}}
            />
          </Box>
    )
}

export default PieDiagram