import { Button, Divider, Menu, MenuItem, Typography } from '@material-ui/core';
import React from 'react';
import { BsDownload } from 'react-icons/bs'
import { GrDocumentPdf } from 'react-icons/gr'
import DownloadList from '../DownloadList';

const DownloadMenu = (props) => {  
  const { components, printRef } = props
  
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const option = [
    {title: 'Report (PDF)', icon: <GrDocumentPdf/>, file: 'pdf'}
  ]

  return (
    <div>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <BsDownload size={16}/>
        <Typography style={{marginLeft: '10px', fontSize: '16px'}}>
          Download Menu
        </Typography>
      </Button>
      <Menu
        id="simple-menu"
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left"
        }}
        keepMounted
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem disabled>Download As</MenuItem>
        <Divider/>
        {
          option.map((val, index) => {
            return (
              <DownloadList
                key = {index}
                title = {val.title}
                icon = {val.icon}
                components = {components}
                printRef = {printRef}
              />
            )
          })
        }
      </Menu>
    </div>
  );
};


export default DownloadMenu;