import {MenuItem, Typography } from "@material-ui/core"
import jsPDF from "jspdf"
import pdf from "html-pdf"
import html2canvas from "html2canvas"
import * as html2img from 'html-to-image'
import { useRef } from "react"

const DownloadList = (props) => {
    const { title, icon, components, printRef } = props

    // const generatePDF = async () => {
        // const html = document.querySelector('#test')
        // const options = { format: 'Letter'}
        // pdf.create(html, options).toFile('./test.pdf', function(err, res){
        //     if(err) return console.log(err)
        //     console.log(res)
        // })
        
    //     const report = new jsPDF ('portrait', 'pt', 'a4')
    //     report.html(document.querySelector('#test')).then(() => {
    //         report.save('report.pdf');
    //     });
    // }

    const handleDownloadPDF = async () => {
        const element = printRef.current
        const canvas = await html2canvas(element)
        const data = canvas.toDataURL('image/jog')
    
        const pdf = new jsPDF()
        const imgProps = pdf.getImageProperties(data)
        const pdfWidth = pdf.internal.pageSize.getWidth()
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width
    
        pdf.addImage(data, 'PNG', 0, 0, pdfWidth, pdfHeight)
        pdf.save('report.pdf')
    }

    return (
        <MenuItem onClick={handleDownloadPDF}>
            {icon}
            <Typography style={{marginLeft: '10px'}}>
                {title}
            </Typography>
        </MenuItem>
    )
}

export default DownloadList