import { Box } from "@material-ui/core"
import ContainerPageDownload from "components/Created/ContainerPageDownload"
import EconomyDiagram from "components/Created/Diagrams/EconomyDiagram"
import NOVDiagram from "components/Created/Diagrams/NOVDiagram"
import PieDiagram from "components/Created/Diagrams/PieDiagram"
import PopulationDiagram from "components/Created/Diagrams/PopulationDiagram"
import { BotFive, TopFive } from "components/Created/Diagrams/RankDiagram"
import VOTDiagram from "components/Created/Diagrams/VOTDiagram"
import VPHDiagram from "components/Created/Diagrams/VPHDiagram"


const DownloadPDF = () => {
    return (
        <Box width='full' padding={10} overflow='scroll' id='test'>
            <ContainerPageDownload
                components ={
                    [
                        <PieDiagram/>, <PopulationDiagram/>, <EconomyDiagram/>, <NOVDiagram/>, <VOTDiagram/>, <VPHDiagram/>, <TopFive/>, <BotFive/>
                    ]
                }
            />

        </Box>
    )
}

export default DownloadPDF