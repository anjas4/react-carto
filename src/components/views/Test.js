import { Grid, Box, Button, Container } from '@material-ui/core';
import PopulationDiagram from "components/Created/Diagrams/PopulationDiagram";
import EconomyDiagram from "components/Created/Diagrams/EconomyDiagram";
import PieDiagram from "components/Created/Diagrams/PieDiagram";
import DownloadMenu from "components/Created/DownloadMenu";
import NOVDiagram from "components/Created/Diagrams/NOVDiagram";
import VOTDiagram from "components/Created/Diagrams/VOTDiagram";
import VPHDiagram from "components/Created/Diagrams/VPHDiagram";
import { TopFive, BotFive } from "components/Created/Diagrams/RankDiagram"
import { useRef } from 'react';

export default function Test() {
  const printRef = useRef()

  return (
    <Box style={{overflow: 'auto', alignItems: "center", justifyContent: "center",}}>
      <div ref={printRef}>
        <Grid container style={{ width: '100%', height: '100%', padding: '10px 10px'}}>
          <Grid style={{width: '30%'}}>
            <Grid>
              <DownloadMenu
                printRef = {printRef}
              />
              <PieDiagram/>
              <PopulationDiagram/>
              <EconomyDiagram/>
            </Grid>
          </Grid>

          <Grid container style={{width: '70%', paddingLeft: '15px'}}>
            <Grid>
              <Grid style={{width: 'full', display: 'flex'}}>
                <NOVDiagram/>
                <VOTDiagram/>
              </Grid>
              
              <VPHDiagram/>

              <Grid style={{width: 'full', display: 'flex'}}>
                <TopFive/>
                <BotFive/>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Box>
  );
}